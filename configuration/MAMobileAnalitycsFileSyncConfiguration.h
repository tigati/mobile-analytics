//
// Created by Timur Gayfulin on 23.10.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAMobileAnalitycsFileSyncConfiguration : NSObject
@property (nonatomic) float timerPeriodHours;

@property (nonatomic) NSString *storageDirectory;
@property (nonatomic) NSString *bundleDirectory;
@property (nonatomic) BOOL skipAutoSync;

@property (nonatomic) NSArray *extraFoldersToSync;
@end
