//
//  MAMobileAnalitycsManagerConfiguration.m
//  mobile-analytics
//
//  Created by Timur Gayfulin on 21.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAMobileAnalitycsManagerConfiguration.h"
#import "MAAppConfigDefaultPublicMigration.h"

@interface MAMobileAnalitycsManagerConfiguration() {
    
}
@end

@implementation MAMobileAnalitycsManagerConfiguration
- (instancetype)init {
    if (self = [super init]) {
        self.configMigrations = @[ [MAAppConfigDefaultPublicMigration class] ];
        self.avaibleForExperiments = YES;
    }
    
    return self;
}

- (NSString *)httpProtocol {
    if (self.useHTTPS) {
        return @"https";
    } else {
        return @"http";
    }
}

@end
