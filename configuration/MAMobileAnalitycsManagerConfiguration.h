//
//  MAMobileAnalitycsManagerConfiguration.h
//  mobile-analytics
//
//  Created by Timur Gayfulin on 21.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MAMobileAnalitycsFileSyncConfiguration;

@interface MAMobileAnalitycsManagerConfiguration : NSObject
@property (nonatomic) NSString *serverDomain;
@property (nonatomic) NSArray* configMigrations;
@property (nonatomic) bool avaibleForExperiments;
@property (nonatomic) MAMobileAnalitycsFileSyncConfiguration *fileSyncConfiguration;
@property (nonatomic) bool useHTTPS;

- (NSString *)httpProtocol;
@end
