//
// Created by Timur Gayfulin on 23.10.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAMobileAnalitycsFileSyncConfiguration.h"


@implementation MAMobileAnalitycsFileSyncConfiguration {

}

- (instancetype)init {
    if (self = [super init]) {
        self.timerPeriodHours = 6;
        self.bundleDirectory = @"FileSync";
        self.storageDirectory = @"FileSync";
        self.extraFoldersToSync = @[];
        self.skipAutoSync = 0;
    }

    return self;
}
@end
