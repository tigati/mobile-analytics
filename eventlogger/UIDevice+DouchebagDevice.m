//
// Created by Timur Gayfulin on 18.11.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "UIDevice+DouchebagDevice.h"
#include <sys/stat.h>

@implementation UIDevice (DouchebagDevice)
static NSString *douchebagString = nil;

+ (BOOL)isDouchebag {
    NSString *result = [self generateDouchebagString];
    if (result) {
#ifdef DEBUG
        return NO;
#else
        return ![result isEqualToString:@"0000"];
#endif
    } else {
        return FALSE;
    }
}

+ (NSString *)isDouchebagString {
    return [self generateDouchebagString];
}

+ (NSString *)generateDouchebagString {
    if (douchebagString != nil) {
        return douchebagString;
    }

    NSMutableString *result = [NSMutableString string];

#if TARGET_IPHONE_SIMULATOR
    [result appendString:@"2"];
#else
    int systemresult = 0;//system(NULL);
    if (systemresult != 0) {
        [result appendString:@"1"];
    } else {
        [result appendString:@"0"];
    }
#endif

    struct stat sb2;
    if (stat("/private/var/mobile/Applications/", &sb2) == -1) {
        [result appendString:@"0"];
    } else {
        [result appendString:@"1"];
    }

    struct stat sb;
    if (stat("/private/var/mobile", &sb) != -1) {
        if (sb.st_mode == 16895) {
            [result appendString:@"1"];
        } else {
            [result appendString:@"0"];
        }
    } else {
        [result appendString:@"2"];
    }

    int pid = fork();
    if (pid == 0) {
        exit(0);
    }

    if (pid == -1) {
        [result appendString:@"0"];
    } else {
        [result appendString:@"1"];
    }

    douchebagString = result;
    return douchebagString;
}


@end
