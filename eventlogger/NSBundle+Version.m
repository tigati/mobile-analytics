//
// Created by Timur Gayfulin on 28.10.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "NSBundle+Version.h"


@implementation NSBundle (Version)
+ (NSString *)applicationVersion {
    NSString* version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if (!version) {
        version = @"None";
    }

    return version;
}
@end
