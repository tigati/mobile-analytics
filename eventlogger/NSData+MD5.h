//
// Created by Timur Gayfulin on 09.08.16.
// Copyright (c) 2016 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (MD5)
- (NSString *)dataHash;
@end
