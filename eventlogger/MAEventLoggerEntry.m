//
//  MAEventLoggerEntry.m
//  eventlogger
//
//  Created by Timur Gayfulin on 08.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "MAEventLoggerEntry.h"

NSString * const MAEventLoggerEntryAddedTimestampKey = @"__added";

@implementation MAEventLoggerEntry
- (NSDictionary *)jsonSerializableDictionary {
    NSMutableDictionary* result = [NSMutableDictionary dictionaryWithDictionary:self.parameters];
    result[MAEventLoggerEntryAddedTimestampKey] = @((int)[self.added timeIntervalSince1970]);
    return result;
}

- (id)initWithJSONDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        NSNumber* addedTimestamp = dictionary[@"added"];
        NSDate* added = [NSDate dateWithTimeIntervalSince1970:addedTimestamp.doubleValue];
        
        self.added = added;
        
        NSMutableDictionary* params = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        [params removeObjectForKey:MAEventLoggerEntryAddedTimestampKey];
        
        self.parameters = params;
    }
    
    return self;
}

+ (NSArray *)serializeEntriesArray:(NSArray *)entries error:(NSError *__autoreleasing *)error {
    NSMutableArray* result = [NSMutableArray arrayWithCapacity:entries.count];
    for (MAEventLoggerEntry* entry in entries) {
        [result addObject:entry.jsonSerializableDictionary];
    }
    
    return result;
}

+ (NSArray *)deserializeEntriesArray:(NSArray *)array error:(NSError *__autoreleasing *)error {
    NSMutableArray* result = [NSMutableArray arrayWithCapacity:array.count];
    for (NSDictionary* entryDict in array) {
        MAEventLoggerEntry* entry = [[MAEventLoggerEntry alloc] initWithJSONDictionary:entryDict];
        [result addObject:entry];
    }
    
    return result;
}

@end
