//
// Created by Timur Gayfulin on 28.10.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Version)
+ (NSString *)applicationVersion;
@end
