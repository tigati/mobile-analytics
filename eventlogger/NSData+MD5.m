//
// Created by Timur Gayfulin on 09.08.16.
// Copyright (c) 2016 7apps.mobi. All rights reserved.
//

#import "NSData+MD5.h"
#include <CommonCrypto/CommonDigest.h>

@implementation NSData (MD5)
- (NSString *)dataHash {
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( self.bytes, (CC_LONG)self.length, digest );

    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];

    for( int i = 0; i < CC_MD5_DIGEST_LENGTH; i++ )
    {
        [output appendFormat:@"%02x", digest[i]];
    }

    return output;
}
@end
