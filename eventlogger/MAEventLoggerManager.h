//
//  MAEventLoggerManager.h
//  mobile-analytics
//
//  Created by Timur Gayfulin on 21.09.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import <UIKit/UIApplication.h>
#import <Foundation/Foundation.h>

@interface MAEventLoggerManager : NSObject
+ (instancetype)sharedInstance;

- (void)addEventWithType:(NSString *)eventType;
- (void)addEventWithType:(NSString *)eventType andParams:(NSDictionary *)params;

- (void)setup;

- (void)flushAndUpload;
@end
