//
//  MAEventLoggerEntry.h
//  eventlogger
//
//  Created by Timur Gayfulin on 08.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAEventLoggerEntry : NSObject
@property (nonatomic) NSDictionary* parameters;
@property (nonatomic) NSDate* added;

- (NSDictionary *)jsonSerializableDictionary;
- (id)initWithJSONDictionary:(NSDictionary *)dictionary;

+ (NSArray *)serializeEntriesArray:(NSArray *)entries error:(NSError *__autoreleasing *)error;
+ (NSArray *)deserializeEntriesArray:(NSArray *)array error:(NSError *__autoreleasing *)error;
@end
