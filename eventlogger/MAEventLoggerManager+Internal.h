//
//  MAEventLoggerManager+Internal.h
//  mobile-analytics
//
//  Created by Timur Gayfulin on 25.09.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAEventLoggerManager.h"

@interface MAEventLoggerManager (Internal)
@property(atomic) NSMutableArray* eventQuery;
@property(atomic) BOOL uploading;
@property(atomic) NSInteger errorCount;
@property(atomic) BOOL waitingUpload;

- (BOOL)flushQuery;
- (void)waitUploadFileIfNeeded;
- (long)maxFilenameNumber;
@end
