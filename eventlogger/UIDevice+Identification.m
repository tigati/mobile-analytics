//
// Created by Timur Gayfulin on 28.10.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "UIDevice+Identification.h"


@implementation UIDevice (Identification)
+ (NSString *)deviceGUID {
    NSString* device_id = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    if (!device_id) {
        device_id = @"Unknown";
    }

    return device_id;
}
@end
