//
// Created by Timur Gayfulin on 09.08.16.
// Copyright (c) 2016 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES256)
- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;
@end
