//
//  MAEventLoggerManager+Internal.m
//  mobile-analytics
//
//  Created by Timur Gayfulin on 25.09.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAEventLoggerManager+Internal.h"

#import "MAMobileAnalitycsManager+Internal.h"
#import "MAMultipartFormData.h"
#import "MAEventLoggerEntry.h"

#import "NSData+GZIP.h"


NSInteger const MAEventLoggerManagerMaxErrors = 10;
NSInteger const MAEventLoggerManagerMaxErrorsPause = 200;

NSString * const MAEventLoggerStorageEventsKey = @"events";
NSString * const MAEventLoggerStorageParamsKey = @"params";

@implementation MAEventLoggerManager(Internal)
@dynamic eventQuery;
@dynamic errorCount;
@dynamic uploading;
@dynamic waitingUpload;

- (long)filenameNumberFindMax:(BOOL)findMax findMin:(BOOL)findMin {
    NSError* error;
    NSString* dirPath = [[MAMobileAnalitycsManager sharedInstance] storageDirectoryAbsolutePath];
    NSArray* filelist = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dirPath error:&error];
    long filenameLong = 0;
    
    if (filelist.count > 0) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        for (NSString* file in filelist) {
            NSNumber* fileNumber = [f numberFromString:file];
            if (fileNumber &&
                (
                 (findMax && fileNumber.longValue > filenameLong) ||
                 (findMin && (fileNumber.longValue < filenameLong || filenameLong == 0))
                 )
                ) {
                filenameLong = fileNumber.longValue;
            }
        }
    }
    
    return filenameLong;
}

- (long)maxFilenameNumber {
    return [self filenameNumberFindMax:YES findMin:NO];
}

- (long)minFilenameNumber {
    return [self filenameNumberFindMax:NO findMin:YES];
}

- (BOOL)canUpload {
    return !self.uploading && self.errorCount < MAEventLoggerManagerMaxErrors;
}

- (void)waitUploadFileIfNeeded {
    if (self.waitingUpload) {
        return;
    }
    
    if (!self.uploading) {
        [self uploadFileIfNeeded];
    } else {
        self.waitingUpload = YES;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.waitingUpload = NO;
            [self waitUploadFileIfNeeded];
        });
    }
}

- (void)uploadFileIfNeeded {
    if (![self canUpload]) {
        return;
    }
    
    long filename = [self minFilenameNumber];
    if (filename == 0) {
        return;
    }
    
    [self uploadFilenameAsync:filename];
}

- (void)removeFile:(long)filename {
    NSString* path = [[[MAMobileAnalitycsManager sharedInstance] storageDirectoryAbsolutePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%ld", filename]];
    NSError* error;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
}

- (NSURL *)apiURL {
    MAMobileAnalitycsManager *maManager = [MAMobileAnalitycsManager sharedInstance];
    NSString* domain = [maManager serverDomain];
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@/events/", maManager.configuration.httpProtocol, domain]];
}

- (NSURL *)uploadURL {
    return [NSURL URLWithString:@"uploader/" relativeToURL:[self apiURL]];
}

- (void)uploadFilename:(long)filename {
    NSString* filenameString = [NSString stringWithFormat:@"%ld", filename];
    NSString* path = [[MAMobileAnalitycsManager sharedInstance] storageDirectoryAbsolutePath];
    path = [path stringByAppendingPathComponent:filenameString];

    NSData* fileData = [NSData dataWithContentsOfFile:path];
    
    NSError* error;
    NSDictionary* queryFileDict = [NSJSONSerialization JSONObjectWithData:fileData options:0 error:&error];
    if (error) {
        self.uploading = FALSE;
        NSLog(@"ERROR deserialization error %@", error);
        [self removeFile:filename];
        return;
    }
    
    NSDictionary* params = queryFileDict[MAEventLoggerStorageParamsKey];
    NSArray* events = queryFileDict[MAEventLoggerStorageEventsKey];
    if (!params || !events) {
        self.uploading = FALSE;
        NSLog(@"ERROR wrong storage format");
        [self removeFile:filename];
        return;
    }
    
    NSData* eventsData = [NSJSONSerialization dataWithJSONObject:events options:0 error:&error];
    if (error) {
        self.uploading = FALSE;
        NSLog(@"ERROR serialization error %@", error);
        [self removeFile:filename];
        return;
    }
    
    NSData *gzippedData = [eventsData gzippedData];
    NSMutableURLRequest* request = [MAMultipartFormData requestWithURL:[self uploadURL]
                                                            parameters:params
                                                                  data:gzippedData
                                                             fieldName:@"eventsFile"];
    
//    NSString* eventsDataString = [[NSString alloc] initWithData:eventsData encoding:NSUTF8StringEncoding];
    NSLog(@"start uploading events data from %@", path);
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               [self processUploadResponse:(NSHTTPURLResponse *)response
                                                   forPath:path
                                                  withData:data
                                                  andError:connectionError];
                           }];
}

- (void)processUploadResponse:(NSHTTPURLResponse *)response
                      forPath:(NSString *)path
                     withData:(NSData *)data
                     andError:(NSError *)connectionError {
    if (response.statusCode == 200 && !connectionError) {
        NSError* error;

        NSLog(@"file %@ succesfully uploaded", path);

        @synchronized(self) {
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        }

        if (!data) {
            NSLog(@"ERROR empty server response");
            return;
        }
    } else {
        NSLog(@"status - %ld, errors count - %ld, %@\n\n%@",
                (long)response.statusCode,
                (long)self.errorCount,
                connectionError,
                [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]
        );

        [self uploadFailed];
    }

    self.uploading = FALSE;
    [self uploadFileIfNeeded];
}

- (void)uploadFilenameAsync:(long)filename {
    self.uploading = TRUE;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self uploadFilename:filename];
    });
}

- (void)uploadFailed {
    self.errorCount++;
    
    if (self.errorCount >= MAEventLoggerManagerMaxErrors) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MAEventLoggerManagerMaxErrorsPause * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.errorCount = 0;
            [self waitUploadFileIfNeeded];
        });
    }
}

- (BOOL)flushQuery {
    NSArray* eventsToFlush;
    
    @synchronized(self.eventQuery) {
        if (self.eventQuery.count == 0) {
            return NO;
        }
        
        eventsToFlush = self.eventQuery;
        self.eventQuery = [NSMutableArray arrayWithCapacity:eventsToFlush.count];
    }
    
    NSError* error;
    NSArray* serializedArray = [MAEventLoggerEntry serializeEntriesArray:eventsToFlush error:&error];
    NSDictionary* idParams = [[MAMobileAnalitycsManager sharedInstance] identificationParams];
    
    NSDictionary* flushedDict = @{
                                  MAEventLoggerStorageEventsKey: serializedArray,
                                  MAEventLoggerStorageParamsKey: idParams
                                  };
    
    NSData* data = [NSJSONSerialization dataWithJSONObject:flushedDict options:0 error:&error];
    if (error) {
        NSLog(@"error saving events query %@", error);
        return NO;
    }
    
    @synchronized(self) {
        long finenameNumber = [self maxFilenameNumber] + 1;
        NSString* filename = [NSString stringWithFormat:@"%ld", finenameNumber];
        NSString *storagePath = [[MAMobileAnalitycsManager sharedInstance] storageDirectoryAbsolutePath];
        NSString* path = [storagePath stringByAppendingPathComponent:filename];
        [data writeToFile:path atomically:NO];
        NSLog(@"file %@ succesfully saved", path);
    }
    
    return YES;
}
@end
