//
// Created by Timur Gayfulin on 09.08.16.
// Copyright (c) 2016 7apps.mobi. All rights reserved.
//

#import "MAAppConfigLoader.h"
#import "MAAppConfigManager+Internal.h"
#import "MAMobileAnalitycsManager+Internal.h"
#import "MAFileSyncManager.h"
#import "NSDictionary+URLEncoding.h"

NSString * const MAMobileAnalitycsManagerResponseSetConfigKey = @"set";
NSString * const MAMobileAnalitycsManagerRequestNoExperimentsKey = @"noExperiments";


@implementation MAAppConfigLoader {
    NSMutableData *responseData;
    NSURLConnection *_connection;
}

- (NSURL *)configURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@/config/",
                    [MAMobileAnalitycsManager sharedInstance].configuration.httpProtocol,
                    [MAMobileAnalitycsManager sharedInstance].serverDomain]
    ];
}

- (void)processConfigResponse {
    if (!responseData) {
        NSLog(@"empty status response");
        return;
    }

    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    NSLog(@"status succesfully loaded %@", responseString);

    NSError *error;
    NSDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    if (error) {
        NSString *dataString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
        [self statusConnectionError:[NSString stringWithFormat:@"status - %@", dataString]];
        return;
    }

    if (![resultDictionary[@"status"] isEqualToString:@"ok"]) {
        [self statusConnectionError:[NSString stringWithFormat:@"response error %@", resultDictionary]];
        return;
    }

    NSDictionary *responseDictionary = resultDictionary[@"response"];
    if (responseDictionary) {
        NSDictionary *configDictionary = responseDictionary[MAMobileAnalitycsManagerResponseSetConfigKey];
        if (configDictionary) {
            [[[MAMobileAnalitycsManager sharedInstance] appConfigManager] saveNewPublicConfig:configDictionary];

            NSArray *newSyncFolders = configDictionary[MAAppConfigExperimentFileSyncFolderKey];
            if (newSyncFolders && [[MAMobileAnalitycsManager sharedInstance] fileSyncInited]) {
                MAFileSyncManager *fileSyncManager = [MAFileSyncManager sharedInstance];
                [fileSyncManager addExtraFolders:newSyncFolders];
                [fileSyncManager waitForResync];
            }
        }
    }
}

- (void)waitForCheckStatus {
    if (_connection) {
        int64_t delay = (int64_t)(2 * NSEC_PER_SEC);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay), dispatch_get_main_queue(), ^{
            [self waitForCheckStatus];
        });
    } else {
        [self checkStatusAsync];
    }
}

- (void)checkStatusAsync {
    NSLog(@"start status request");

    NSDictionary *identificationParams = [[MAMobileAnalitycsManager sharedInstance] identificationParams];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:identificationParams];

    if (![[MAMobileAnalitycsManager sharedInstance] avaibleForExperiments]) {
        params[MAMobileAnalitycsManagerRequestNoExperimentsKey] = @(YES);
    }

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[self configURL]];

    request.HTTPBody = [params.queryString dataUsingEncoding:NSUTF8StringEncoding];
    request.HTTPMethod = @"POST";

    _connection = [NSURLConnection connectionWithRequest:request delegate:self];
    [_connection start];
}

- (void)stopChecking {
    if (_connection) {
        [_connection cancel];
    }

    responseData = nil;
    _connection = nil;
}

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse *)response;
    if (httpResponse.statusCode != 200) {
        [self statusConnectionError:[NSString stringWithFormat:@"ERROR wrong code: %ld", (long)httpResponse.statusCode]];
    } else {
        responseData = [[NSMutableData alloc]init];
    }
}
- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data
{
    [responseData appendData:data];
}
- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
    [self statusConnectionError:error.localizedDescription];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    _connection = nil;
    [self processConfigResponse];
}

- (void)statusConnectionError:(NSString *)error {
    responseData = nil;
    _connection = nil;
    NSLog(@"status connection error %@", error);
}

@end
