//
//  MAEventLoggerManager.m
//  eventlogger
//
//  Created by Timur Gayfulin on 08.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "MAEventLoggerManager.h"
#import "MAEventLoggerEntry.h"
#import "MAMobileAnalyticsManager.h"
#import "MAMobileAnalitycsManager+Internal.h"
#import "MAEventLoggerManager+Internal.h"
#import "MAAppConfigManager+Internal.h"
#import <sys/utsname.h>
#import "NSBundle+Version.h"
#import "UIDevice+DouchebagDevice.h"

NSTimeInterval const MAEventLoggerManagerFlushPeriod = 60;
NSTimeInterval const MAEventLoggerManagerUploadPeriod = 120;
NSTimeInterval const MAEventLoggerManagerHighFrequencyUploadPeriod = 15;

NSString * const MAEventLoggerEventTypeKey = @"__event_type";

@interface MAEventLoggerManager()
@property(atomic) NSMutableArray* eventQuery;

@property(atomic) BOOL uploading;
@property(atomic) NSInteger errorCount;
@property(atomic) BOOL waitingUpload;
@end

@implementation MAEventLoggerManager {
    BOOL _setupFinished;
}

+ (instancetype)sharedInstance
{
    static MAEventLoggerManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MAEventLoggerManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.eventQuery = [NSMutableArray array];
    }
    
    return self;
}

- (void)startStandartTimers {
    [NSTimer scheduledTimerWithTimeInterval:MAEventLoggerManagerFlushPeriod
                                     target:self
                                   selector:@selector(flushQuery:)
                                   userInfo:nil
                                    repeats:YES];
    
    [NSTimer scheduledTimerWithTimeInterval:MAEventLoggerManagerUploadPeriod
                                     target:self
                                   selector:@selector(flushAndUpload:)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)setup {
    if (_setupFinished) {
        return;
    }
    _setupFinished = TRUE;
    
    for (NSString* notificationName in @[UIApplicationWillResignActiveNotification, UIApplicationWillTerminateNotification]) {
        [[NSNotificationCenter defaultCenter] addObserverForName:notificationName
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          [self flushQuery];
                                                      }];
    }
    
    if ([[[MAMobileAnalitycsManager sharedInstance] appConfigManager] sessionNumber].integerValue == 0) {
        NSTimer* hfUpload = [NSTimer scheduledTimerWithTimeInterval:MAEventLoggerManagerHighFrequencyUploadPeriod
                                                             target:self
                                                           selector:@selector(flushAndUpload:)
                                                           userInfo:nil
                                                            repeats:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MAEventLoggerManagerUploadPeriod * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [hfUpload invalidate];
            [self startStandartTimers];
        });
    } else {
        [self startStandartTimers];
    }

    [self sendAppStartedEventAndUpdateSessionCount];
    [self checkBundleVersion];
    [self flushAndUpload];
}

- (void)checkBundleVersion {
    MAAppConfigManager* configManager = [[MAMobileAnalitycsManager sharedInstance] appConfigManager];
    NSString *savedVersion = [configManager getPrivateAppConfig][MAAppConfigManagerPrivateAppConfigBundleVersionKey];
    NSString *actualVersion = [NSBundle applicationVersion];

    if (![savedVersion isEqualToString:actualVersion]) {
        [[MAEventLoggerManager sharedInstance] addEventWithType:@"APP_VERSION_UPDATED"
                                                      andParams:@{
                                                              @"OLD_VERSION": savedVersion,
                                                              @"NEW_VERSION": actualVersion
                                                      }];

        [configManager saveAndUpdatePrivateConfigKey:MAAppConfigManagerPrivateAppConfigBundleVersionKey
                                           withValue:actualVersion];
    }
}

- (void)sendAppStartedEventAndUpdateSessionCount {
#ifdef DEBUG
    NSString *target = @"DEBUG";
#else
    NSString *target = @"RELEASE";
#endif

    struct utsname systemInfo;
    uname(&systemInfo);

    NSString* deviceName = [NSString stringWithCString:systemInfo.machine
                                              encoding:NSUTF8StringEncoding];

    MAAppConfigManager* configManager = [[MAMobileAnalitycsManager sharedInstance] appConfigManager];
    
    NSNumber* sessionNumber = @([configManager sessionNumber].intValue + 1);

    [configManager saveAndUpdatePrivateConfigKey:MAAppConfigManagerPrivateAppConfigSessionNumberKey
                                       withValue:sessionNumber];

    NSDictionary *eventParams = @{
        @"SESSION_COUNTER" : sessionNumber,
        @"MODEL"           : [UIDevice currentDevice].model,
        @"DEVICE"          : deviceName,
        @"SYSTEM_NAME"     : [UIDevice currentDevice].systemName,
        @"SYSTEM_VERSION"  : [UIDevice currentDevice].systemVersion,
        @"BUNDLE_VERSION"  : [NSBundle applicationVersion],
        @"LOCALE"          : [NSLocale currentLocale].localeIdentifier,
        @"APP_CONFIG"      : [[MAMobileAnalitycsManager sharedInstance] appConfig],
        @"TARGET"          : target,
        @"IS_DOUCHEBAG"    : [UIDevice isDouchebagString]
    };
    [[MAEventLoggerManager sharedInstance] addEventWithType:@"APP_STARTED"
                                                  andParams:eventParams];
}

- (void)addEvent:(NSDictionary *)params {
    MAEventLoggerEntry* entry = [[MAEventLoggerEntry alloc] init];
    entry.parameters = params;
    entry.added = [[NSDate alloc] init];

    @synchronized(self.eventQuery) {
        [self.eventQuery addObject:entry];
    }
}

- (void)addEventWithType:(NSString *)eventType {
    [self addEventWithType:eventType andParams:nil];
}

- (void)addEventWithType:(NSString *)eventType andParams:(NSDictionary *)params {
    NSMutableDictionary* mutableParams;
    if (params) {
        mutableParams = [NSMutableDictionary dictionaryWithDictionary:params];
    } else {
        mutableParams = [NSMutableDictionary dictionary];
    }
    
    mutableParams[MAEventLoggerEventTypeKey] = eventType;
    [self addEvent:mutableParams];
}

- (void)flushQuery:(NSTimer *)timer {
    [self flushQuery];
}

- (void)flushAndUpload {
    [self flushQuery];
    [self waitUploadFileIfNeeded];
}

- (void)flushAndUpload:(NSTimer *)timer {
    [self flushAndUpload];
}

- (void)uploadFile:(NSTimer *)timer {
    [self waitUploadFileIfNeeded];
}

@end
