//
// Created by Timur Gayfulin on 18.11.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIDevice (DouchebagDevice)
+ (BOOL)isDouchebag;
+ (NSString *)isDouchebagString;
@end
