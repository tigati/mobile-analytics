# IOS Mobile Analytics

MAFileSync
==========

Система синхронизации файлов. Для подключения системы необходимо при инициализации Mobile Analytics задать конфиг синхронизации

`MAMobileAnalitycsManagerConfiguration* config = [[MAMobileAnalitycsManagerConfiguration alloc] init]; 
 config.fileSyncConfiguration = [[MAMobileAnalitycsFileSyncConfiguration alloc] init];`
 
Доступные параметры конфигурации (_MAMobileAnalitycsManagerConfiguration_)

* **storageDirectory** (_NSString_) рабочая директория для FileSync внутри папки Documents установленного приложения, в которую будут загружаться файлы.
По умолчанию - FileSync

* **bundleDirectory**  (_NSString_) директория для FileSync внутри bundle приложения откуда приложение будет устанавливать файлы.
По умолчанию - FileSync

* **extraFoldersToSync** (_NSArray_) массив строк поддиректорий storageDirectory включённых в синхронизацию. 
По умолчанию - @[]

* **timerPeriodHours** (_float_) приложение будет синхронизоваться и при запуске и раз в указанное количество часов.  
По умолчанию - 6

* **skipAutoSync** (_BOOL_) не запускать синхронизацию файлов автоматически при старте
По умолчанию - FALSE

При первом запуске приложения библитека скопирует файлы в storageDirectory из bundleDirectory. Для поддержки синхронизации с сервером необходимо добавить хотя бы одну папку в extraFoldersToSync


Методы
------

`[[MAFileSyncManager sharedInstance] addExtraFolders:@[@"extra"]]`
Используется для того чтобы добавить папки в синхронизацию после запуска приложения

`[[MAFileSyncManager sharedInstance] waitForResync]`
Запускает процесс синхронизации. Если процесс уже запущен, то дожидается окончания и запускается заново

`[[MAFileSyncManager sharedInstance] checkFileSyncStorageDirectory]`
Проверяет все ли подпапки из storageDirectory перенесены в bundleDirectory. При необходимости копирует недостающие

События
-------

FileSyncManager информирует о состоянии загрузок через NSNotificationCenter. Подписаться на событие можно например так

`[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fileDownloaded:) name:MAAppFileSyncStatusFileDownloaded object:nil];`

Список событий
--------------

* **MAAppFileSyncStatusStartSync** - начало синхронизации
* **MAAppFileSyncStatusSyncSuccess** - синхронизация успешно завершена
* **MAAppFileSyncStatusSyncError** - синхронизация завершена с ошибками
* **MAAppFileSyncStatusFileDownloaded** - файл загружен
* **MAAppFileSyncStatusFileDownloadFailed** - загрузка файла завершилась ошибкой


**MAAppFileSyncStatusFileDownloaded** в userInfo нотификации содержит информацию о файле и состоянии закачки. Используются следующие ключи

* _MAAppFileSyncStatusFileKeyFilename_ (NSString) имя файла (относительно storageDirectory, то есть включает в себя подпапку)
* _MAAppFileSyncStatusFileKeyDownloadedFiles_ (NSNumber) файлов скачано
* _MAAppFileSyncStatusFileKeyTotalFiles_ (NSNumber) необходимо скачать всего


**MAAppFileSyncStatusFileDownloadFailed** в userInfo нотификации содержит информацию о файле и ошибке

* _MAAppFileSyncStatusFileKeyFilename_ (NSString) имя файла 
* _MAAppFileSyncStatusFileKeyError_ (NSString) ошибка закачки
