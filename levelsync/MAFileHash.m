//
//  MALevelSyncFileHash.m
//  levelsync
//
//  Created by Timur Gayfulin on 09.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "MAFileHash.h"
#import "NSData+MD5.h"

@implementation MAFileHash

+(NSString *)md5HashOfPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // Make sure the file exists
    if( [fileManager fileExistsAtPath:path isDirectory:nil] )
    {
        NSData *data = [NSData dataWithContentsOfFile:path];
        return [data dataHash];
    }
    else
    {
        return @"";
    }
}

+ (NSString *)hashOfPath:(NSString *)path {
    return [self md5HashOfPath:path];
}

+(NSString *)shaHashOfPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    // Make sure the file exists
    if( [fileManager fileExistsAtPath:path isDirectory:nil] )
    {
        NSData *data = [NSData dataWithContentsOfFile:path];
        unsigned char digest[CC_SHA1_DIGEST_LENGTH];
        CC_SHA1( data.bytes, (CC_LONG)data.length, digest );
        
        NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
        
        for( int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++ )
        {
            [output appendFormat:@"%02x", digest[i]];
        }
        
        return output;
    }
    else
    {
        return @"";
    }
}

@end
