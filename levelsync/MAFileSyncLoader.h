//
//  MALevelSyncLoader.h
//  levelsync
//
//  Created by Timur Gayfulin on 02.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

#define MAFileSyncLoaderMaxConnections 5

@interface MAFileSyncLoader : NSObject
@property (nonatomic) NSMutableArray* downloadQueue;

- (void)run;
- (BOOL)isRunning;
@end
