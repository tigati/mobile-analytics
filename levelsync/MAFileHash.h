//
//  MALevelSyncFileHash.h
//  levelsync
//
//  Created by Timur Gayfulin on 09.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//


#import <Foundation/Foundation.h>
#include <CommonCrypto/CommonDigest.h>

@interface MAFileHash : NSObject {
}

/**
 * Get the md5 hash of a file
 *
 * @returns		NSString
 * @param		path		Full path to the file
 */
+(NSString *)md5HashOfPath:(NSString *)path;
+(NSString *)hashOfPath:(NSString *)path;

/**
 * Get the sha1 hash of a file
 *
 * @returns		NSString
 * @param		path		Full path to the file
 */
+(NSString *)shaHashOfPath:(NSString *)path;

@end
