//
//  MALevelSyncManager.m
//  MALevelSyncManager
//
//  Created by Timur Gayfulin on 02.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "MAFileSyncManager.h"
#import "MAMobileAnalyticsManager.h"
#import "MAFileSyncLoader.h"
#import "MAFileSyncManager+Internal.h"

NSString * const MAAppConfigExperimentFileSyncFolderKey = @"__experiment_filesync_folder";

NSString * const MAAppFileSyncStatusStartSync = @"__MAAppFileSyncStatusStartSync";
NSString * const MAAppFileSyncStatusSyncSuccess = @"__MAAppFileSyncStatusSyncSuccess";
NSString * const MAAppFileSyncStatusFileDownloaded = @"__MAAppFileSyncStatusFileDownloaded";
NSString * const MAAppFileSyncStatusSyncError = @"__MAAppFileSyncStatusSyncError";
NSString * const MAAppFileSyncStatusFileDownloadFailed = @"__MAAppFileSyncStatusFileDownloadFailed";

NSString * const MAAppFileSyncStatusFileKeyFilename = @"filename";
NSString * const MAAppFileSyncStatusFileKeyTotalFiles = @"total";
NSString * const MAAppFileSyncStatusFileKeyDownloadedFiles = @"downloaded";
NSString * const MAAppFileSyncStatusFileKeyError = @"error";

@interface MAFileSyncManager() {
    BOOL initialized;
}
@property (nonatomic) MAFileSyncLoader* loader;
@property (nonatomic) NSMutableArray *extraFoldersToSync;
@end

@implementation MAFileSyncManager

+ (instancetype)sharedInstance
{
    static MAFileSyncManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MAFileSyncManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
        self.extraFoldersToSync = [NSMutableArray array];
    }

    return self;
}

- (BOOL)initialized {
    return initialized;
}


- (void)setup {
    if (initialized) {
        return;
    }

    if (self.config) {
        [self checkFileSyncStorageDirectory];
    }

    initialized = TRUE;
}

- (BOOL)addExtraFolders:(NSArray *)folders {
    BOOL foldersAdded = FALSE;
    for (NSArray *folder in folders) {
        if (![self.extraFoldersToSync containsObject:folder]) {
            [[self extraFoldersToSync] addObject:folder];
            foldersAdded = TRUE;
        }
    }

    return foldersAdded;
}

- (void)waitForResync {
    if (!initialized || [self.loader isRunning]) {
        dispatch_time_t pause = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
        dispatch_after(pause, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [self waitForResync];
        });
    } else {
        [self fileListSynchronization];
    }
}

- (void)syncFiles:(NSTimer *)timer {
    [self fileListSynchronization];
}

- (void)run {
    if (!initialized) {
        [self setup];
    }

    if (!self.config.skipAutoSync) {
        [self fileListSynchronization];
    }    

    [NSTimer scheduledTimerWithTimeInterval:[[self config] timerPeriodHours] * 60 * 60
                                     target:self
                                   selector:@selector(syncFiles:)
                                   userInfo:nil
                                    repeats:YES];
}

- (NSURL *)rootAPIURL {
    MAMobileAnalitycsManager *maManager = [MAMobileAnalitycsManager sharedInstance];
    NSString *domain = [maManager serverDomain];
    NSString *stringURL = [NSString stringWithFormat:@"%@://%@/filesync/", maManager.configuration.httpProtocol, domain];
    return [NSURL URLWithString:stringURL];
}

- (void)fileListSynchronization {
    if (!self.loader) {
        self.loader = [[MAFileSyncLoader alloc] init];
    }

    [self.loader run];
}

- (void)excludeDirectoryFromBackup {
    NSURL* storageDirURL= [NSURL fileURLWithPath:[self storageDirectoryAbsolutePath]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:[storageDirURL path]]) {
        NSError *error = nil;
        [storageDirURL setResourceValue:@YES forKey:NSURLIsExcludedFromBackupKey error:&error];
    }
}

- (void)checkFileSyncStorageDirectory {
    NSFileManager* fileManager = [NSFileManager defaultManager];

    NSString *sourcePath = [[NSBundle mainBundle] resourcePath];
    sourcePath = [sourcePath stringByAppendingPathComponent:self.config.bundleDirectory];

    NSError *error;
    NSArray *sourceSubDirs = [fileManager contentsOfDirectoryAtPath:sourcePath
                                                              error:&error];

    if (![fileManager fileExistsAtPath:[self storageDirectoryAbsolutePath]]) {
        NSError* createError;
        [[NSFileManager defaultManager] createDirectoryAtPath:[self storageDirectoryAbsolutePath]
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:&createError];

        [self excludeDirectoryFromBackup];
    }

    if (error) {
        NSLog(@"error accessing bundle: %@", error);
        return;
    }

    for (NSString *subDirectory in sourceSubDirs) {
        NSString *subDirAbsolutePath = [sourcePath stringByAppendingPathComponent:subDirectory];
        NSString *subDirDestination = [self fileAbsolutePathRelativeToStorage:subDirectory];
        if ([fileManager fileExistsAtPath:subDirDestination]) {
            NSLog(@"found %@ skipping %@", subDirDestination, subDirectory);
            continue;
        }
        
        
        [self copyFilesFrom:subDirAbsolutePath to:subDirDestination];
    }
}

- (NSString *)storageDirectoryAbsolutePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dataPath = [paths[0] stringByAppendingPathComponent:self.config.storageDirectory];
    
    return dataPath;
}

- (NSString *)fileWorkingVersion:(NSString *)filename {
    NSEnumerator *foldersEnumerator = [[self foldersToSync] reverseObjectEnumerator];
    for (NSString *folderName in foldersEnumerator) {
        NSString *path = [self fileVersionAbsolutePath:filename fromFolder:folderName];
        if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
            return path;
        }
    }

    return nil;
}

- (NSString *)fileVersionAbsolutePath:(NSString *)filename fromFolder:(NSString *)folder {
    NSString *relativePath = [NSString stringWithFormat:@"%@/%@", folder, filename];
    return [self fileAbsolutePathRelativeToStorage:relativePath];
}

//- (NSString *)fileDefaultVersion:(NSString *)filename {
//    return [self fileVersionAbsolutePath:filename fromFolder:MAAppFileSyncDefaultDirectory];
//}

- (NSString *)fileAbsolutePathRelativeToStorage:(NSString *)filename {
    return [[self storageDirectoryAbsolutePath] stringByAppendingPathComponent:filename];
}

- (void)clearStorage {
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:[self storageDirectoryAbsolutePath] error:&error];
}

- (NSArray *)extraFoldersFromExperiment {
    NSDictionary *appConfig = [[MAMobileAnalitycsManager sharedInstance] appConfig];
    id experimentFolders = appConfig[MAAppConfigExperimentFileSyncFolderKey];

    if (experimentFolders) {
        return experimentFolders;
    } else {
        return @[];
    }
}
@end
