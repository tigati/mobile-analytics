//
// Created by Timur Gayfulin on 23.10.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAFileSyncManager.h"
#import "MAMobileAnalitycsFileSyncConfiguration.h"

@interface MAFileSyncManager (Internal)
- (MAMobileAnalitycsFileSyncConfiguration *)config;
- (NSArray *)foldersToSync;

- (void)copyFilesFrom:(NSString *)source to:(NSString *)destination;
- (void)moveFilesFrom:(NSString *)source to:(NSString *)destination;

@property (nonatomic) NSMutableArray *extraFoldersToSync;
@end
