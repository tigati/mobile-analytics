//
//  MALevelSyncInfoLoader.m
//  levelsync
//
//  Created by Timur Gayfulin on 02.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "MAFileSyncLoader.h"
#import "MAFileSyncManager.h"
#import "MAMobileAnalyticsManager.h"
#import "MAFileSyncManager+Internal.h"

@interface MAFileSyncLoader() {
    dispatch_queue_t queueSync;
}
@property (nonatomic) NSURLConnection* connection;
@property (nonatomic) NSString* apiURL;
@property (atomic) NSInteger downloadConnectionsCount;
@property (atomic) NSInteger downloadErrorsCount;
@property (atomic) NSInteger totalDownloads;
@property (atomic) BOOL isRunning;
@end

@implementation MAFileSyncLoader
-(instancetype)init {
    if (self = [super init]) {
        queueSync = dispatch_queue_create("mobi.7apps.fileSync", NULL);
    }
    
    return self;
}

- (void)run {
    if (self.isRunning) {
        return;
    }

    self.isRunning = TRUE;
    
    self.totalDownloads = 0;
    self.downloadErrorsCount = 0;
    self.downloadConnectionsCount = 0;

    NSData *requestData = [self requestBody];
    if (!requestData) {
        NSLog(@"ERROR empty request data");
        return;
    }

//    NSString *requestString = [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];

    NSURL* url = [[MAFileSyncManager sharedInstance] rootAPIURL];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = requestData;
    request.timeoutInterval = 20;

//    NSString* requestBody = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
//    NSLog(@"sending to url %@\n%@", url);
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               [self processFilesyncResponse:(NSHTTPURLResponse *)response
                                                    withData:data
                                                    andError:connectionError];
                           }];

    [[NSNotificationCenter defaultCenter] postNotificationName:MAAppFileSyncStatusStartSync object:self];
}

- (void)fileStatusConnectionFailed:(NSString *)message {
    NSLog(@"ERROR file status connection failed %@", message);
    [self fileSyncFailed];
}

- (void)fileSyncFailed {
    self.isRunning = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:MAAppFileSyncStatusSyncError object:self];
}

- (void)fileSyncSuccessed {
    NSString* storagePath = [[MAFileSyncManager sharedInstance] storageDirectoryAbsolutePath];
    NSString* tempPath = [self tempDownloadFolder];
    
    [[MAFileSyncManager sharedInstance] moveFilesFrom:tempPath to:storagePath];
    NSError* error;
    [[NSFileManager defaultManager] removeItemAtPath:tempPath error:&error];
    
    self.isRunning = NO;
    NSLog(@"file sync finished");
    [[NSNotificationCenter defaultCenter] postNotificationName:MAAppFileSyncStatusSyncSuccess object:self];
}


- (void)processFilesyncResponse:(NSHTTPURLResponse *)response
                       withData:(NSData *)data
                       andError:(NSError *)connectionError {
    if (connectionError == nil && response.statusCode == 200) {
        if (data.length > 0) {
            NSError* error;
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            if (error) {
                [self fileStatusConnectionFailed:error.description];

                return;
            } else {
                self.apiURL = responseDict[@"url"];
                if (!self.apiURL) {
                    [self fileStatusConnectionFailed:@"no api url"];

                    return;
                }

                NSArray* files = responseDict[@"files"];
                
                self.downloadQueue = [NSMutableArray arrayWithArray:files];
                self.totalDownloads = self.downloadQueue.count;

                if (self.totalDownloads > 0) {
                    NSLog(@"making download queue %@", self.downloadQueue);
                    self.downloadErrorsCount = 0;
                    [self startConnections];
                } else {
                    [self fileSyncSuccessed];
                }
            }
        } else {
            [self fileSyncSuccessed];
        }
    } else {
        NSString *message = [NSString stringWithFormat:@"%@, status = %ld", connectionError.description, (long)response.statusCode];
        [self fileStatusConnectionFailed:message];
    }
}

- (NSString *)tempDownloadFolder {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *folderPath = [cachePath stringByAppendingPathComponent:@"TempDownloads"];
    return folderPath;
}

- (void)startConnections {
    dispatch_async(queueSync, ^{
        while (YES) {
            BOOL result = [self startConnection];
            if (!result) {
                break;
            }
        }
    });
}

- (BOOL)startConnection {
    if (self.downloadConnectionsCount >= MAFileSyncLoaderMaxConnections) {
        // это ограничение максимального количества соединений
        return FALSE;
    }
    
    if (self.downloadQueue.count == 0) {
        return FALSE;
    }
    
    NSString* filename = [self.downloadQueue lastObject];
    [self.downloadQueue removeLastObject];
        
    MAMobileAnalitycsManager *maManager = [MAMobileAnalitycsManager sharedInstance];
    NSString* domain = [maManager serverDomain];
    NSURL* baseUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@", maManager.configuration.httpProtocol, domain]];
    NSCharacterSet *allowedCharacterSet = [NSCharacterSet URLPathAllowedCharacterSet];
    NSString *fileURL = [filename stringByAddingPercentEncodingWithAllowedCharacters:allowedCharacterSet];

    NSString* relativeFileURL = [NSString stringWithFormat:@"%@%@", self.apiURL, fileURL];
    NSURL* url = [NSURL URLWithString:relativeFileURL relativeToURL:baseUrl];

    NSLog(@"starting downloading %@ from %@", filename, url);

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    request.timeoutInterval = 20;

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               [self processDownloadResponse:(NSHTTPURLResponse *)response
                                                 forFilename:filename
                                                    withData:data
                                                    andError:connectionError];
                           }];

    self.downloadConnectionsCount++;
    
    return TRUE;
}

- (void)processDownloadResponse:(NSHTTPURLResponse *)response
                    forFilename:(NSString *)filename
                       withData:(NSData *)data
                       andError:(NSError *)connectionError {

    BOOL downloaded = FALSE;
    NSString *errorString = @"";

    if ([data length] > 0 && connectionError == nil && response.statusCode == 200)
    {
        NSString* tempFileAbsolutePath = [[self tempDownloadFolder] stringByAppendingPathComponent:filename];
        NSString* directoryPath = [tempFileAbsolutePath stringByDeletingLastPathComponent];
        NSFileManager* fileManager = [NSFileManager defaultManager];
        NSError* error;

        if (![fileManager fileExistsAtPath:directoryPath]) {
            [fileManager createDirectoryAtPath:directoryPath
                   withIntermediateDirectories:YES
                                    attributes:nil
                                         error:&error];

            if (error) {
                errorString = [NSString stringWithFormat:@"ERROR directory create %@", error];
            }
        }

        if (!error) {
            [data writeToFile:tempFileAbsolutePath atomically:YES];
            NSLog(@"Loaded %@ written to %@", filename, tempFileAbsolutePath);
            downloaded = TRUE;
        }
    } else if ([data length] == 0 && connectionError == nil) {
        errorString = @"ERROR: Nothing was downloaded.";
    } else if (connectionError != nil){
        errorString = [NSString stringWithFormat:@"ERROR = %@", connectionError];
    } else if (response.statusCode != 200) {
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        errorString = [NSString stringWithFormat:@"ERROR wrong answer status: %ld. %@",
                        (long)response.statusCode,
                        responseString];
    }
    
    dispatch_async(queueSync, ^{
        self.downloadConnectionsCount--;
        NSInteger filesRemaining = [self filesRemaining];
        NSInteger downloadedFiles = self.totalDownloads - filesRemaining - self.downloadErrorsCount;

        if (downloaded) {
            NSLog(@"%d of %d downloaded", downloadedFiles, self.totalDownloads);
            [[NSNotificationCenter defaultCenter] postNotificationName:MAAppFileSyncStatusFileDownloaded
                                                                object:self
                                                              userInfo:@{
                                                                      MAAppFileSyncStatusFileKeyFilename: filename,
                                                                      MAAppFileSyncStatusFileKeyTotalFiles: @(self.totalDownloads),
                                                                      MAAppFileSyncStatusFileKeyDownloadedFiles: @(downloadedFiles)
                                                              }];
        } else {
            self.downloadErrorsCount++;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:MAAppFileSyncStatusFileDownloadFailed
                                                                object:self
                                                              userInfo:@{
                                                                      MAAppFileSyncStatusFileKeyFilename: filename,
                                                                      MAAppFileSyncStatusFileKeyError: errorString
                                                              }];
        }

        if (self.downloadErrorsCount == 0) {
            if (filesRemaining > 0) {
                [self startConnections];
            } else {
                [self fileSyncSuccessed];
            }
        } else if (self.downloadConnectionsCount == 0) {
            [self fileSyncFailed];
        }
    });
}

- (NSInteger) filesRemaining {
    return self.downloadQueue.count + self.downloadConnectionsCount;
};

- (NSMutableDictionary *)fileHashesFromFolder:(NSString *)folder {
    NSMutableDictionary *fileHashes = [[NSMutableDictionary alloc] init];
    
    NSDirectoryEnumerator *directoryEnumerator = [[NSFileManager defaultManager] enumeratorAtPath:folder];
    for (NSString* filename in directoryEnumerator) {
        NSString *absPath = [folder stringByAppendingPathComponent:filename];
        BOOL isDirectory;
        if ([[NSFileManager defaultManager] fileExistsAtPath:absPath isDirectory:&isDirectory]) {
            if (isDirectory) {
                continue;
            }
            
            NSString* fileHash = [MAFileHash hashOfPath:absPath];
            fileHashes[filename] = fileHash;
        }
    }
    return fileHashes;
}

- (NSData *)requestBody {
    NSString* storagePath = [[MAFileSyncManager sharedInstance] storageDirectoryAbsolutePath];
    NSMutableDictionary *files = [self fileHashesFromFolder:storagePath];
    
    NSString* tempPath = [self tempDownloadFolder];
    if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath]) {
        NSMutableDictionary* tempFilesHashes = [self fileHashesFromFolder:tempPath];
        [files addEntriesFromDictionary:tempFilesHashes];
    }

    NSDictionary *request = @{
        @"folders": [[MAFileSyncManager sharedInstance] foldersToSync],
        @"files": files
    };

//    NSLog(@"fileSync request %@", filesMDateParts);
    NSLog(@"fileSync request %lu files", (unsigned long)files.count);

    NSError *error;
    return [NSJSONSerialization dataWithJSONObject:request options:0 error:&error];
}

@end
