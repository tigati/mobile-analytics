//
//  MALevelSyncManager.h
//  MALevelSyncManager
//
//  Created by Timur Gayfulin on 02.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAFileHash.h"

FOUNDATION_EXPORT NSString * const MAAppConfigExperimentFileSyncFolderKey;

FOUNDATION_EXPORT NSString * const MAAppFileSyncStatusStartSync;
FOUNDATION_EXPORT NSString * const MAAppFileSyncStatusSyncSuccess;
FOUNDATION_EXPORT NSString * const MAAppFileSyncStatusFileDownloaded;
FOUNDATION_EXPORT NSString * const MAAppFileSyncStatusFileDownloadFailed;
FOUNDATION_EXPORT NSString * const MAAppFileSyncStatusSyncError;

FOUNDATION_EXPORT NSString * const MAAppFileSyncStatusFileKeyFilename;
FOUNDATION_EXPORT NSString * const MAAppFileSyncStatusFileKeyTotalFiles;
FOUNDATION_EXPORT NSString * const MAAppFileSyncStatusFileKeyDownloadedFiles;
FOUNDATION_EXPORT NSString * const MAAppFileSyncStatusFileKeyError;


@interface MAFileSyncManager : NSObject
+ (instancetype)sharedInstance;


/*!
 * @brief Инициализация системы синхронизации папки
 */
- (void)setup;
- (void)run;
- (BOOL)initialized;

- (void)waitForResync;

- (BOOL)addExtraFolders:(NSArray *)folders;

- (NSURL *)rootAPIURL;
- (NSString *)fileWorkingVersion:(NSString *)filename;

- (NSString *)fileAbsolutePathRelativeToStorage:(NSString *)filename;
- (NSString *)storageDirectoryAbsolutePath;

- (void)clearStorage;
- (void)checkFileSyncStorageDirectory;
- (void)excludeDirectoryFromBackup;

- (NSArray *)extraFoldersFromExperiment;
@end
