//
// Created by Timur Gayfulin on 23.10.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "MAFileSyncManager+Internal.h"
#import "MAMobileAnalyticsManager.h"


@implementation MAFileSyncManager (Internal)
@dynamic extraFoldersToSync;

- (MAMobileAnalitycsFileSyncConfiguration *)config {
    return [[[MAMobileAnalitycsManager sharedInstance] configuration] fileSyncConfiguration];
}

- (NSArray *)foldersToSync {
//    NSMutableArray *folders = [@[@"default"] mutableCopy];
    NSMutableArray *folders = [[NSMutableArray alloc] init];

    for (NSString *folder in self.config.extraFoldersToSync) {
        if (![folders containsObject:folder]) {
            [folders addObject:folder];
        }
    }

    for (NSString *folder in self.extraFoldersFromExperiment) {
        if (![folders containsObject:folder]) {
            [folders addObject:folder];
        }
    }

    for (NSString *folder in self.extraFoldersToSync) {
        if (![folders containsObject:folder]) {
            [folders addObject:folder];
        }
    }

    return folders;
}

- (void)transferFilesFrom:(NSString *)source to:(NSString *)destination copy:(BOOL)copy {
    NSFileManager* fileManager = [NSFileManager defaultManager];
    NSDirectoryEnumerator *directoryEnumerator = [fileManager enumeratorAtPath:source];
    for (NSString* filename in directoryEnumerator) {
        NSString* filepath = [source stringByAppendingPathComponent:filename];
        BOOL isDirectory;
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filepath isDirectory:&isDirectory]) {
            if (isDirectory) {
                continue;
            }
            
            NSString* destinationFilepath = [destination stringByAppendingPathComponent:filename];
            
            NSError *error2;
            NSString *directory = [destinationFilepath stringByDeletingLastPathComponent];
            if (![fileManager fileExistsAtPath:directory]) {
                [fileManager createDirectoryAtPath:directory
                       withIntermediateDirectories:YES
                                        attributes:nil
                                             error:&error2];
            }
            
            NSError *error1;
            
            if ([fileManager fileExistsAtPath:destinationFilepath]) {
                NSError* error3;
                [fileManager removeItemAtPath:destinationFilepath error:&error3];
            }
            
            if (copy) {
                [fileManager copyItemAtPath:filepath toPath:destinationFilepath error:&error1];
            } else {
                [fileManager moveItemAtPath:filepath toPath:destinationFilepath error:&error1];
            }
        }
    }
}

- (void)moveFilesFrom:(NSString *)source to:(NSString *)destination {
    [self transferFilesFrom:source to:destination copy:FALSE];
}

- (void)copyFilesFrom:(NSString *)source to:(NSString *)destination {
    [self transferFilesFrom:source to:destination copy:TRUE];
}
@end
