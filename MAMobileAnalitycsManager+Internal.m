//
//  MAMobileAnalitycsManager+Internal.m
//  mobile-analytics
//
//  Created by Timur Gayfulin on 25.09.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import <UIKit/UIDevice.h>
#import "MAMobileAnalitycsManager+Internal.h"
#import "MAAppConfigManager.h"
#import "MAFileSyncManager.h"
#import "NSDictionary+URLEncoding.h"
#import "NSBundle+Version.h"
#import "UIDevice+Identification.h"

NSString * const MAMobileAnalitycsManagerDocumentsDirectory = @"MobileAnalytics";

@implementation MAMobileAnalitycsManager (Internal)
@dynamic appConfigManager;

- (NSString *)storageDirectoryAbsolutePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:MAMobileAnalitycsManagerDocumentsDirectory];
    
    return dataPath;
}

- (void)excludeDirectoriesFromBackup {
    NSURL* storageDirURL= [NSURL fileURLWithPath:[self storageDirectoryAbsolutePath]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:[storageDirURL path]]) {
        NSError *error = nil;
        [storageDirURL setResourceValue:@YES forKey:NSURLIsExcludedFromBackupKey error:&error];
    }
}

- (NSDictionary *)identificationParams {
    NSString* language = [NSLocale preferredLanguages][0];
    if (!language) {
        language = @"-";
    }

    return @{
            @"user_guid": [[MAMobileAnalitycsManager sharedInstance] getUserGUID],
            @"device_id": [UIDevice deviceGUID],
            @"bundle_version": [NSBundle applicationVersion],
            @"device_language": language
    };
}

@end
