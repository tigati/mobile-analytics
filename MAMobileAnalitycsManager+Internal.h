//
//  MAMobileAnalitycsManager+Internal.h
//  mobile-analytics
//
//  Created by Timur Gayfulin on 25.09.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAMobileAnalyticsManager.h"
@class MAAppConfigManager;

@interface MAMobileAnalitycsManager (Internal)
- (NSDictionary *)identificationParams;
- (NSString *)storageDirectoryAbsolutePath;
- (void)excludeDirectoriesFromBackup;

@property(nonatomic) MAAppConfigManager* appConfigManager;
@end
