//
//  MAMobileAnalitycsManager.m
//  eventlogger
//
//  Created by Timur Gayfulin on 08.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "MAMobileAnalyticsManager.h"
#import "MAEventLoggerManager.h"
#import "MAMobileAnalitycsManager+Internal.h"
#import "MAAppConfigManager.h"
#import "MAFileSyncManager.h"
#import "MAAppConfigLoader.h"
#import <CrashReporter/CrashReporter.h>

@interface MAMobileAnalitycsManager() {
}
@property(nonatomic) MAAppConfigLoader* appConfigLoader;
@property(nonatomic) MAAppConfigManager* appConfigManager;
@property(nonatomic) MAMobileAnalitycsManagerConfiguration* configuration;
@end


@implementation MAMobileAnalitycsManager
+ (instancetype)sharedInstance {
    static MAMobileAnalitycsManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MAMobileAnalitycsManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if (self = [super init]) {
    }
    
    return self;
}

- (void)handleCrashReport {
    PLCrashReporter *crashReporter = [PLCrashReporter sharedReporter];
    NSData *crashData;
    NSError *error;
    
    // Try loading the crash report
    crashData = [crashReporter loadPendingCrashReportDataAndReturnError:&error];
    if (crashData == nil) {
        NSLog(@"Could not load crash report: %@", error);
    } else {
        PLCrashReport *report = [[PLCrashReport alloc] initWithData:crashData error:&error];
        if (report == nil) {
            NSLog(@"Could not parse crash report");
        } else {
            NSLog(@"Found new crash! What a shame!");
            NSString* reportData = [PLCrashReportTextFormatter stringValueForCrashReport:report
                                                                          withTextFormat:PLCrashReportTextFormatiOS];

            [[MAEventLoggerManager sharedInstance] addEventWithType:@"CRASHED"
                                                          andParams:@{
                                                              @"REPORT": reportData
                                                          }];

            [[MAEventLoggerManager sharedInstance] flushAndUpload];
        }
    }
    
    // Purge the report
    [crashReporter purgePendingCrashReport];
}

- (void)setup {
    MAMobileAnalitycsManagerConfiguration* configuration = [[MAMobileAnalitycsManagerConfiguration alloc] init];
    [self setupWithConfiguration:configuration];
}

- (NSString *)serverDomain {
    return self.configuration.serverDomain;
}

- (void)setupCrashReporter {
    PLCrashReporter *crashReporter = [PLCrashReporter sharedReporter];
    NSError *error;

    if ([crashReporter hasPendingCrashReport]) {
        [self handleCrashReport];
    }

    // Enable the Crash Reporter
    if (![crashReporter enableCrashReporterAndReturnError:&error]) {
        NSLog(@"Warning: Could not enable crash reporter: %@", error);
    }
}

- (void)setupWithConfiguration:(MAMobileAnalitycsManagerConfiguration *)configuration {
    if (self.configuration) {
        return;
    }
    
    self.configuration = configuration;

    [self checkStorageDirectory];
    self.appConfigManager = [[MAAppConfigManager alloc] initWithMigrationHistory:self.configuration.configMigrations];
    
    [self setupCrashReporter];
    
    [[MAEventLoggerManager sharedInstance] setup];

    if (self.configuration.fileSyncConfiguration) {
        [[MAFileSyncManager sharedInstance] run];        
    }

    _avaibleForExperiments = self.configuration.avaibleForExperiments;

    self.appConfigLoader = [[MAAppConfigLoader alloc] init];
    [self.appConfigLoader waitForCheckStatus];

    NSLog(@"Mobile Analytics inited");
}

- (void)setAvaibleForExperiments:(BOOL)avaibleForExperiments {
    if (avaibleForExperiments == _avaibleForExperiments) {
        return;
    }

    if (_avaibleForExperiments) {
        [self.appConfigLoader stopChecking];
    }

    if (avaibleForExperiments) {
        [self.appConfigLoader waitForCheckStatus];
    }

    _avaibleForExperiments = avaibleForExperiments;
}

- (BOOL)fileSyncInited {
    return self.configuration.fileSyncConfiguration != NULL;
}

- (void)checkStorageDirectory {
    NSString* dataPath = [self storageDirectoryAbsolutePath];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        NSError* error;
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath
                                  withIntermediateDirectories:NO
                                                   attributes:nil
                                                        error:&error];

        if (error) {
            NSLog(@"ERROR creating %@: %@", dataPath, error);
        } else {
            NSLog(@"%@ sucsessfully created", dataPath);
        }

        [self excludeDirectoriesFromBackup];
    }

    NSLog(@"Mobile Analytics starting init with %@", dataPath);
}

- (NSString *)getUserGUID {
    return [self.appConfigManager getUserGUID];
}

- (void)updateUserGUID:(NSString *)userGUID {
    return [self.appConfigManager updateUserGUID:userGUID];
}

- (NSDictionary *)appConfig {
    return [self.appConfigManager getAppConfig];
}

- (BOOL)updateConfigIfNeeded {
    return [[self appConfigManager] updatePublicConfigIfNeeded];
}


@end
