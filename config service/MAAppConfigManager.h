//
//  AppConfigManager.h
//  mobile-analytics
//
//  Created by Timur Gayfulin on 12.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAAppConfigManager : NSObject
- (instancetype)initWithMigrationHistory:(NSArray *)migrations;
+ (NSDictionary *)defaultConfigWithMigrationHistory:(NSArray *)migrations;

- (NSDictionary *)getAppConfig;

- (void)updateUserGUID:(NSString *)userGUID;
- (NSString *)getUserGUID;

- (void)saveNewPublicConfig:(NSDictionary *)config;
- (BOOL)updatePublicConfigIfNeeded;

- (NSNumber *)sessionNumber;
@end
