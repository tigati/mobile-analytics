//
//  MAAppConfigMigrationProtocol.h
//  mobile-analytics
//
//  Created by Timur Gayfulin on 12.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MAAppConfigMigrationProtocol <NSObject>
+ (NSInteger)configVersion;
+ (NSDictionary *)migrateConfig:(NSDictionary *)oldConfig;
@end
