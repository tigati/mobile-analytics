//
//  AppConfigManager.m
//  mobile-analytics
//
//  Created by Timur Gayfulin on 12.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAEventLoggerManager.h"
#import "MAEventLoggerManager+Internal.h"
#import "MAAppConfigManager.h"
#import "MAAppConfigManager+Internal.h"
#import "UIDevice+Identification.h"
#import "MAAppConfigPrivateMigration1.h"
#import "MAAppConfigPrivateMigration2.h"
#import "MAAppConfigPrivateMigration3.h"
#import "MAAppConfigPrivateMigration4.h"
#import "MAAppConfigPrivateMigration5.h"
#import "UIDevice+Identification.h"
#import "NSData+AES256.h"

NSString * const MAAppConfigManagerPublicAppConfigFilename = @"Config";

NSString * const MAAppConfigVersionKey = @"__ConfigVersion";

@interface MAAppConfigManager()
@property (atomic) NSDictionary* publicAppConfig;
@property (atomic) NSDictionary* privateAppConfig;
@end

@implementation MAAppConfigManager
- (instancetype)initWithMigrationHistory:(NSArray *)migrations {
    if (self = [super init]) {
        [self preparePrivateConfig];
        
        self.publicAppConfig = [self loadConfig:MAAppConfigManagerPublicAppConfigFilename];

        NSDictionary* migratedPublicAppConfig = [MAAppConfigManager migrateConfigIfNeeded:self.publicAppConfig
                                                                           withMigrations:migrations];
        [self saveAndUpdatePublicConfigIfNeeded:migratedPublicAppConfig];

        NSLog(@"Public config: %@", migratedPublicAppConfig);
        NSLog(@"Device GUID: %@", [UIDevice deviceGUID]);
        NSLog(@"MAAppConfigManager inited");
    }
    
    return self;
}

- (void)preparePrivateConfig {
    self.privateAppConfig = [self loadConfig:MAAppConfigManagerPrivateAppConfigFilename];
    NSArray* privateMigrations = @[
        [MAAppConfigPrivateMigration1 class],
        [MAAppConfigPrivateMigration2 class],
        [MAAppConfigPrivateMigration3 class],
        [MAAppConfigPrivateMigration4 class],
        [MAAppConfigPrivateMigration5 class]
    ];
    
    NSDictionary* migratedPrivateAppConfig = [MAAppConfigManager migrateConfigIfNeeded:self.privateAppConfig
                                                                        withMigrations:privateMigrations];
    [self saveAndUpdatePrivateConfigIfNeeded:migratedPrivateAppConfig];

    NSLog(@"Private config:%@", migratedPrivateAppConfig);
}

- (void)updateUserGUID:(NSString *)userGUID {
    if (userGUID == nil) {
        NSLog(@"Error in updateUserGUID: userGUID == nil");

        return;
    }

    NSString* oldGUID = self.privateAppConfig[MAAppConfigManagerPrivateAppConfigUserGUIDKey];
    if (oldGUID) {
        MAEventLoggerManager* logger = [MAEventLoggerManager sharedInstance];
        
        [logger addEventWithType:@"USER_ID_CHANGED" andParams:@{
                                                                @"OLD_GUID": oldGUID,
                                                                @"NEW_GUID": userGUID
                                                                }];
        
        [logger flushQuery];
    }

    [self saveAndUpdatePrivateConfigKey:MAAppConfigManagerPrivateAppConfigUserGUIDKey
                              withValue:userGUID];
}

+ (NSDictionary *)migrateConfigIfNeeded:(NSDictionary *)config withMigrations:(NSArray *)migrations {
    NSNumber* configVersionNumber = config[MAAppConfigVersionKey];
    NSInteger configVersionInteger = 0;
    if (configVersionNumber) {
        configVersionInteger = configVersionNumber.integerValue;
    }
    
    for (Class migrationClass in migrations) {
        NSInteger migrationVersion = [(id<MAAppConfigMigrationProtocol>)migrationClass configVersion];

        BOOL needToMigrate = FALSE;
        if (!config) {
            needToMigrate = TRUE;
        } else if (configVersionInteger < migrationVersion) {
            needToMigrate = TRUE;
        }
        
        if (!needToMigrate) {
            continue;
        }
        
        NSDictionary* newConfig = [(id<MAAppConfigMigrationProtocol>)migrationClass migrateConfig:config];
        
        NSMutableDictionary* migratedConfig = [NSMutableDictionary dictionaryWithDictionary:newConfig];
        migratedConfig[MAAppConfigVersionKey] = @(migrationVersion);
        
        return [self migrateConfigIfNeeded:migratedConfig withMigrations:migrations];
    }
    
    return config;
}

+ (NSDictionary *)defaultConfigWithMigrationHistory:(NSArray *)migrations {
    return [MAAppConfigManager migrateConfigIfNeeded:nil withMigrations:migrations];
}

- (NSString *)getUserGUID {
    return self.privateAppConfig[MAAppConfigManagerPrivateAppConfigUserGUIDKey];
}

- (BOOL)savePublicConfigIfNeeded:(NSDictionary *)config {
    if (!config) {
        return FALSE;
    }
    
    if ([config isEqualToDictionary:self.publicAppConfig]) {
        return FALSE;
    }
    
    [self saveConfig:config intoFilename:MAAppConfigManagerPublicAppConfigFilename];
    return TRUE;
}

- (BOOL)saveAndUpdatePublicConfigIfNeeded:(NSDictionary *)config {
    BOOL updated = [self savePublicConfigIfNeeded:config];
    if (updated) {
        self.publicAppConfig = config;
    }
    
    return updated;
}

- (void)saveNewPublicConfig:(NSDictionary *)config {
    NSNumber *currentConfigVersion = self.publicAppConfig[MAAppConfigVersionKey];
    
    NSMutableDictionary* newConfig = [NSMutableDictionary dictionaryWithDictionary:config];
    newConfig[MAAppConfigVersionKey] = currentConfigVersion;

    [self savePublicConfigIfNeeded:newConfig];
}

- (BOOL)updatePublicConfigIfNeeded {
    NSDictionary *savedConfig = [self loadConfig:MAAppConfigManagerPublicAppConfigFilename];
    if ([savedConfig isEqualToDictionary:[self publicAppConfig]]) {
        return FALSE;
    } else {
        self.publicAppConfig = savedConfig;
        NSLog(@"config updated\n%@", savedConfig);
        return TRUE;
    }
}

- (NSDictionary *)loadConfig:(NSString *)filename {
    NSString* configPath = [self savedConfigAbsolutePath:filename];
    BOOL isDirectory = NO;
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:configPath isDirectory:&isDirectory];
    if (exists && !isDirectory) {
        NSData* configData = [NSData dataWithContentsOfFile:configPath];
        if (configData) {
            NSError* error;
            NSDictionary* config = [NSJSONSerialization JSONObjectWithData:configData options:0 error:&error];
            [self saveConfig:config intoFilename:filename];
        }

        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:configPath error:&error];
    }

    NSString *encConfigPath = [self savedEncConfigAbsolutePath:filename];
    BOOL encExists = [[NSFileManager defaultManager] fileExistsAtPath:encConfigPath];
    if (encExists) {
        NSData *encData = [NSData dataWithContentsOfFile:encConfigPath];
        if (encData) {
            NSError* error;
            NSData *decrypted = [encData AES256DecryptWithKey:@"ECRYPTLOL"];

            NSDictionary* config = [NSJSONSerialization JSONObjectWithData:decrypted options:0 error:&error];
            return config;
        }
    }
    
    return nil;
}

- (NSNumber *)sessionNumber {
    NSNumber* sessionNumber = [self getPrivateAppConfig][MAAppConfigManagerPrivateAppConfigSessionNumberKey];
    return sessionNumber;
}

- (NSDictionary *)getAppConfig {
    return self.publicAppConfig;
}

@end
