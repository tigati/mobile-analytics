//
//  MAAppConfigManager+Internal.h
//  mobile-analytics
//
//  Created by Timur Gayfulin on 12.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAAppConfigManager.h"

FOUNDATION_EXPORT NSString *const MAAppConfigManagerPrivateAppConfigUserGUIDKey;
FOUNDATION_EXPORT NSString *const MAAppConfigManagerPrivateAppConfigSessionNumberKey;
FOUNDATION_EXPORT NSString *const MAAppConfigManagerPrivateAppConfigBundleVersionKey;

FOUNDATION_EXPORT NSString *const MAAppConfigManagerPrivateAppConfigFilename;

@interface MAAppConfigManager (Internal)
@property (atomic) NSDictionary* privateAppConfig;

- (NSDictionary *)getPrivateAppConfig;

- (NSString *)savedConfigAbsolutePath:(NSString *)filename;
- (NSString *)savedEncConfigAbsolutePath:(NSString *)filename;

- (void)saveConfig:(NSDictionary *)config intoFilename:(NSString *)filename;

- (BOOL)saveAndUpdatePrivateConfigIfNeeded:(NSDictionary *)config;
- (BOOL)saveAndUpdatePrivateConfigKey:(NSString *)key withValue:(id)value;
@end
