//
//  MAAppConfigManager+Internal.m
//  mobile-analytics
//
//  Created by Timur Gayfulin on 12.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAAppConfigManager+Internal.h"
#import "MAMobileAnalitycsManager+Internal.h"
#import "NSData+MD5.h"
#import "NSData+AES256.h"

NSString * const MAAppConfigManagerPrivateAppConfigUserGUIDKey = @"UserGUID";
NSString * const MAAppConfigManagerPrivateAppConfigSessionNumberKey = @"SessionNumber";
NSString * const MAAppConfigManagerPrivateAppConfigBundleVersionKey = @"BundleVersion";

NSString * const MAAppConfigManagerPrivateAppConfigFilename = @"PrivateConfig";

@implementation MAAppConfigManager (Internal)
@dynamic privateAppConfig;

- (NSDictionary *)getPrivateAppConfig {
    return self.privateAppConfig;
}

- (NSString *)encConfigFilename:(NSString *)filename {
    return [[filename dataUsingEncoding:NSUTF8StringEncoding] dataHash];
}

- (NSString *)savedEncConfigAbsolutePath:(NSString *)filename {
    return [self savedConfigAbsolutePath:[self encConfigFilename:filename]];
}

- (NSString *)savedConfigAbsolutePath:(NSString *)filename {
    NSString * directory = [[MAMobileAnalitycsManager sharedInstance] storageDirectoryAbsolutePath];
    return [directory stringByAppendingPathComponent:filename];
}

- (void)saveConfig:(NSDictionary *)config intoFilename:(NSString *)filename {
    NSError* error;
    NSData* configJSON = [NSJSONSerialization dataWithJSONObject:config options:0 error:&error];

    configJSON = [configJSON AES256EncryptWithKey:@"ECRYPTLOL"];
    [configJSON writeToFile:[self savedEncConfigAbsolutePath:filename] atomically:YES];
}

- (BOOL)saveAndUpdatePrivateConfigIfNeeded:(NSDictionary *)config {
    if (!config) {
        return FALSE;
    }

    if ([config isEqualToDictionary:self.privateAppConfig]) {
        return FALSE;
    }

    self.privateAppConfig = config;
    [self saveConfig:config intoFilename:MAAppConfigManagerPrivateAppConfigFilename];

    return TRUE;
}

- (BOOL)saveAndUpdatePrivateConfigKey:(NSString *)key withValue:(id)value {
    NSMutableDictionary* mutableConfig = [NSMutableDictionary dictionaryWithDictionary:self.privateAppConfig];
    mutableConfig[key] = value;
    return [self saveAndUpdatePrivateConfigIfNeeded:mutableConfig];
}

@end
