//
//  MAAppConfigPrivateMigration1.m
//  mobile-analytics
//
//  Created by Timur Gayfulin on 12.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAAppConfigPrivateMigration1.h"
#import "MAAppConfigManager+Internal.h"

@implementation MAAppConfigPrivateMigration1
+ (NSInteger)configVersion {
    return 1;
}

+ (NSDictionary *)migrateConfig:(NSDictionary *)oldConfig {
    return @{
             MAAppConfigManagerPrivateAppConfigUserGUIDKey: [[NSUUID UUID] UUIDString]
             };
}
@end
