//
//  MAAppConfigDefaultPublicMigration.m
//  mobile-analytics
//
//  Created by Timur Gayfulin on 12.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAAppConfigDefaultPublicMigration.h"

@implementation MAAppConfigDefaultPublicMigration
+ (NSInteger)configVersion {
    return 0;
}

+ (NSDictionary *)migrateConfig:(NSDictionary *)oldConfig {
    return @{};
}
@end
