//
// Created by Timur Gayfulin on 29.12.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAAppConfigMigrationProtocol.h"


@interface MAAppConfigPrivateMigration4 : NSObject<MAAppConfigMigrationProtocol>
@end
