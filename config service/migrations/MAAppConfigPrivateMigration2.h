//
//  MAAppConfigPrivateMigration1.h
//  mobile-analytics
//
//  Created by Timur Gayfulin on 12.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAAppConfigMigrationProtocol.h"

@interface MAAppConfigPrivateMigration2 : NSObject<MAAppConfigMigrationProtocol>
@end
