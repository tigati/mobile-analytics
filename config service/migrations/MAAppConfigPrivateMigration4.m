//
// Created by Timur Gayfulin on 29.12.15.
// Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "MAAppConfigPrivateMigration4.h"
#import "MAFileSyncManager.h"


@implementation MAAppConfigPrivateMigration4
+ (NSInteger)configVersion {
    return 4;
}

+ (NSDictionary *)migrateConfig:(NSDictionary *)oldConfig {
    if ([[MAFileSyncManager sharedInstance] initialized]) {
        [[MAFileSyncManager sharedInstance] clearStorage];
        [[MAFileSyncManager sharedInstance] checkFileSyncStorageDirectory];
    }

    return oldConfig;
}
@end
