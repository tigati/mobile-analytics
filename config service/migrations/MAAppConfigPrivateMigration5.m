//
// Created by Timur Gayfulin on 30.08.16.
// Copyright (c) 2016 7apps.mobi. All rights reserved.
//

#import "MAAppConfigPrivateMigration5.h"
#import "MAFileSyncManager.h"
#import "MAMobileAnalitycsManager+Internal.h"


@implementation MAAppConfigPrivateMigration5
+ (NSInteger)configVersion {
    return 5;
}

+ (NSDictionary *)migrateConfig:(NSDictionary *)oldConfig {
    [[MAFileSyncManager sharedInstance] excludeDirectoryFromBackup];
    [[MAMobileAnalitycsManager sharedInstance] excludeDirectoriesFromBackup];
    return oldConfig;
}
@end
