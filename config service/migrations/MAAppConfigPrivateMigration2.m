//
//  MAAppConfigPrivateMigration1.m
//  mobile-analytics
//
//  Created by Timur Gayfulin on 12.10.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "MAAppConfigManager+Internal.h"
#import "MAAppConfigPrivateMigration2.h"

@implementation MAAppConfigPrivateMigration2
+ (NSInteger)configVersion {
    return 1;
}

+ (NSDictionary *)migrateConfig:(NSDictionary *)oldConfig {
    NSMutableDictionary* newConfig = [NSMutableDictionary dictionaryWithDictionary:oldConfig];
    newConfig[MAAppConfigManagerPrivateAppConfigSessionNumberKey] = @(0);
    return newConfig;
}
@end
