//
// Created by Timur Gayfulin on 30.08.16.
// Copyright (c) 2016 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAAppConfigMigrationProtocol.h"


@interface MAAppConfigPrivateMigration5 : NSObject<MAAppConfigMigrationProtocol>
@end
