//
// Created by Timur Gayfulin on 28.10.15.
// Copyright (c) 2015 Apportable. All rights reserved.
//

#import "MAAppConfigPrivateMigration3.h"
#import "MAAppConfigManager+Internal.h"
#import "NSBundle+Version.h"


@implementation MAAppConfigPrivateMigration3
+ (NSInteger)configVersion {
    return 3;
}

+ (NSDictionary *)migrateConfig:(NSDictionary *)oldConfig {
    NSMutableDictionary* newConfig = [NSMutableDictionary dictionaryWithDictionary:oldConfig];
    newConfig[MAAppConfigManagerPrivateAppConfigBundleVersionKey] = [NSBundle applicationVersion];
    return newConfig;
}
@end
