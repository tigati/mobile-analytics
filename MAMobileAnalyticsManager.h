//
//  MAMobileAnalitycsManager
//  eventlogger
//
//  Created by Timur Gayfulin on 08.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MAMobileAnalitycsManagerConfiguration.h"
#import "MAMobileAnalitycsFileSyncConfiguration.h"

@class MAAppConfigManager;

@interface MAMobileAnalitycsManager : NSObject
+ (instancetype)sharedInstance;

@property (nonatomic, readonly) NSString *serverDomain;
@property (nonatomic, readonly) MAAppConfigManager* appConfigManager;
@property (nonatomic, readonly) MAMobileAnalitycsManagerConfiguration* configuration;
@property (nonatomic) BOOL avaibleForExperiments;

- (void)updateUserGUID:(NSString *)userGUID;
- (NSString *)getUserGUID;

- (NSDictionary *)appConfig;
- (BOOL)updateConfigIfNeeded;

- (void)setupWithConfiguration:(MAMobileAnalitycsManagerConfiguration *)configuration;
- (void)setup;

- (BOOL)fileSyncInited;
@end
