//
//  MAMultipartFormData.m
//  eventlogger
//
//  Created by Timur Gayfulin on 15.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import "MAMultipartFormData.h"

@implementation MAMultipartFormData
+ (NSMutableURLRequest *)requestWithURL:(NSURL *)url
                             parameters:(NSDictionary *)parameters
                                   data:(NSData *)data
                              fieldName:(NSString *)fieldName
{
    NSString* boundary = [MAMultipartFormData generateBoundaryString];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    NSData* body = [MAMultipartFormData createBodyWithBoundary:boundary
                                                    parameters:parameters
                                                          data:data
                                                     fieldName:fieldName];
    
    [request setHTTPBody:body];
    [request setHTTPMethod:@"POST"];
  
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    return request;
}

+ (NSString *)generateBoundaryString
{
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
}

+ (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                              data:(NSData *)data
                          fieldName:(NSString *)fieldName
{
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    NSString *mimetype = @"application/data";
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, fieldName] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
    [httpBody appendData:data];
    [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}
@end
