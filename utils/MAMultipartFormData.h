//
//  MAMultipartFormData.h
//  eventlogger
//
//  Created by Timur Gayfulin on 15.09.15.
//  Copyright (c) 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAMultipartFormData : NSObject
+ (NSMutableURLRequest *)requestWithURL:(NSURL *)url
                             parameters:(NSDictionary *)parameters
                                   data:(NSData *)data
                              fieldName:(NSString *)fieldName;
@end
