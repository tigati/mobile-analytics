//
//  NSDictionary+NSDictionary_UrlEncoding.m
//  mobile-analytics
//
//  Created by Timur Gayfulin on 25.09.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import "NSDictionary+URLEncoding.h"

static NSString *urlEscapeString(NSString *unencodedString)
{
    CFStringRef originalStringRef = (__bridge_retained CFStringRef)unencodedString;
    NSString *s = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,originalStringRef, NULL, NULL,kCFStringEncodingUTF8);
    CFRelease(originalStringRef);
    return s;
}

@implementation NSDictionary (URLEncoding)

- (NSString *)queryStringToUrlString:(NSString *)urlString {
    NSMutableString *urlWithQuerystring = [[NSMutableString alloc] initWithString:urlString];
    if ([urlWithQuerystring rangeOfString:@"?"].location == NSNotFound) {
        [urlWithQuerystring appendFormat:@"?%@", self.queryString];
    } else {
        [urlWithQuerystring appendFormat:@"&%@", self.queryString];
    }
    return urlWithQuerystring;
}

- (NSString *)queryString {
    NSMutableArray* params = [NSMutableArray arrayWithCapacity:self.count];
    for (id key in self) {
        NSString *keyString = [key description];
        NSString *valueString = [[self objectForKey:key] description];
        
        [params addObject:[NSString stringWithFormat:@"%@=%@", urlEscapeString(keyString), urlEscapeString(valueString)]];
    }
    
    return [params componentsJoinedByString:@"&"];
}

@end
