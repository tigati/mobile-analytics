//
//  NSDictionary+NSDictionary_UrlEncoding.h
//  mobile-analytics
//
//  Created by Timur Gayfulin on 25.09.15.
//  Copyright © 2015 7apps.mobi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (URLEncoding)
- (NSString *)queryStringToUrlString:(NSString *)urlString;
- (NSString *)queryString;
@end
